﻿using System;

namespace Practice.Data
{
    public class Patient
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Diagnosis { get; set; }
        public string Cure { get; set; }
        public Chamber Chamber { get; set; }
        public Doctor Doctor { get; set; }
        public DateTime Arrival { get; set; }
        public string InsuranceId { get; set; }
    }
}
