﻿using Microsoft.EntityFrameworkCore;

namespace Practice.Data
{
    class DataContainer : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Chamber> Chambers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Practice;Trusted_Connection=True;");
        }
    }
}
