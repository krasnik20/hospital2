﻿namespace Practice.Data
{
    public class Chamber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public Doctor Doctor { get; set; }
    }
}
