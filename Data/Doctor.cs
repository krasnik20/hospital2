﻿namespace Practice.Data
{
    public class Doctor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Speciality { get; set; }
        public string Description { get; set; }
    }
}
