﻿using Practice.Data;

namespace Practice
{
    static class Extensions
    {
        public static string GetName(this Patient patient)
        {
            return $"{patient.LastName} {patient.MiddleName} {patient.FirstName}";
        }

        public static string GetName(this Doctor doctor)
        {
            return doctor == null ? "Нет врача" : $"{doctor.LastName} {doctor.MiddleName} {doctor.FirstName}";
        }

        public static string GetNumber(this Chamber chamber)
        {
            return chamber == null ? "Нет палаты" : chamber.Number;
        }
    }
}
