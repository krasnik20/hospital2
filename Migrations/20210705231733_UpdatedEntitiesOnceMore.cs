﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Practice.Migrations
{
    public partial class UpdatedEntitiesOnceMore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Diseases",
                table: "Patients",
                newName: "Diagnosis");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Diagnosis",
                table: "Patients",
                newName: "Diseases");
        }
    }
}
