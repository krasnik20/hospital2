﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Practice.Migrations
{
    public partial class UpdatedEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Chambers_RoomId",
                table: "Patients");

            migrationBuilder.RenameColumn(
                name: "RoomId",
                table: "Patients",
                newName: "ChamberId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_RoomId",
                table: "Patients",
                newName: "IX_Patients_ChamberId");

            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "Doctors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Chambers_ChamberId",
                table: "Patients",
                column: "ChamberId",
                principalTable: "Chambers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Chambers_ChamberId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "MiddleName",
                table: "Doctors");

            migrationBuilder.RenameColumn(
                name: "ChamberId",
                table: "Patients",
                newName: "RoomId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_ChamberId",
                table: "Patients",
                newName: "IX_Patients_RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Chambers_RoomId",
                table: "Patients",
                column: "RoomId",
                principalTable: "Chambers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
