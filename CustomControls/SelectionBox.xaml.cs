﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Practice.CustomControls
{
    public partial class SelectionBox : UserControl
    {
        public string Text
        {
            get
            {
                return Info.Text;
            }
            set
            {
                Info.Text = value;
            }
        }

        public SelectionBox()
        {
            InitializeComponent();
        }

        public event EventHandler EditPressed;
        public event EventHandler RemovePressed;

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            EditPressed?.Invoke(this, null);
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RemovePressed?.Invoke(this, null);
        }
    }
}
