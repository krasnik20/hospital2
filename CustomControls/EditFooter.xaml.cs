﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Practice.CustomControls
{
    public partial class EditFooter : UserControl
    {
        public bool CanDelete { set { RemoveButton.Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public EditFooter()
        {
            InitializeComponent();
            CanDelete = true;
        }

        public event EventHandler SavePressed;
        public event EventHandler RemovePressed;

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SavePressed?.Invoke(sender,e);
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RemovePressed?.Invoke(sender, e);
        }
    }
}
