﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Practice.CustomControls
{
    public partial class Paginator : UserControl
    {
        public int PageNumber { get; set; } = 1;
        public Paginator()
        {
            InitializeComponent();
        }

        public event EventHandler NextPressed;
        public event EventHandler PreviousPressed;

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            if (--PageNumber == 0) PageNumber++;
            NumberLabel.Text = PageNumber.ToString();
            NextPressed?.Invoke(this, null);
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            PageNumber++;
            NumberLabel.Text = PageNumber.ToString();
            PreviousPressed?.Invoke(this, null);
        }

    }
}
