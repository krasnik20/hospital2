﻿using Practice.Views;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Practice
{
    public partial class MainWindow : Window
    {
        private Dictionary<string, Page> pages;
        public MainWindow()
        {
            InitializeComponent();
            pages = new Dictionary<string, Page>();
            pages.Add(nameof(PatientsPage), new PatientsPage());
            pages.Add(nameof(DoctorsPage), new DoctorsPage());
            pages.Add(nameof(ChambersPage), new ChambersPage());
            MainFrame.Content = pages.First().Value;

        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            string targetPageName;
            if (sender == PatientsButton)
                targetPageName = nameof(PatientsPage);
            else if (sender == DoctorsButton)
                targetPageName = nameof(DoctorsPage);
            else targetPageName = nameof(ChambersPage);
            MainFrame.Content = pages[targetPageName];
        }
    }
}
