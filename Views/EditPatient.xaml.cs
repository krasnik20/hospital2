﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class EditPatient : Page
    {
        private Patient patient = new Patient();
        private DataContainer dataContainer;

        public int PatientId
        {
            get
            {
                return patient.Id;
            }
            set
            {
                UpdateFields(value);
            }
        }

        private async Task UpdateFields(int value)
        {
            patient = await dataContainer.Patients.FirstOrDefaultAsync(p => p.Id == value);
            await dataContainer.Entry(patient).Reference(p => p.Doctor).LoadAsync();
            await dataContainer.Entry(patient).Reference(p => p.Chamber).LoadAsync();
            (LastName.Text, MiddleName.Text, FirstName.Text, Diagnosis.Text, Cure.Text, Arrival.Text) = (patient.LastName, patient.MiddleName, patient.FirstName, patient.Diagnosis, patient.Cure, patient.Arrival.ToShortDateString());
            DoctorSelection.Text = patient.Doctor?.GetName() ?? "Не выбран";
            ChamberSelection.Text = patient.Chamber?.GetNumber() ?? "Не выбрана";
            Footer.CanDelete = true;
        }

        public EditPatient()
        {
            InitializeComponent();
            dataContainer = new DataContainer();
        }

        private async void Doctor_EditPressed(object sender, EventArgs e)
        {
            var page = new DoctorsPage() { SelectionMode = true };
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(page);
            var doctor = await page.Result.Task;
            if (doctor == null) return;
            patient.Doctor = doctor;
            DoctorSelection.Text = doctor.GetName();
        }

        private void Doctor_RemovePressed(object sender, EventArgs e)
        {
            patient.Doctor = null;
            DoctorSelection.Text = "Не выбран";
        }

        private async void Chamber_EditPressed(object sender, EventArgs e)
        {
            var page = new ChambersPage() { SelectionMode = true };
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(page);
            var chamber = await page.Result.Task;
            if (chamber == null) return;
            patient.Chamber = chamber;
            ChamberSelection.Text = chamber.GetNumber();
        }

        private void Chamber_RemovePressed(object sender, EventArgs e)
        {
            patient.Chamber = null;
            ChamberSelection.Text = "Не выбрана";
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            try
            {
                patient.Arrival = DateTime.Parse(Arrival.Text);
            }
            catch
            {
                MessageBox.Show("Неправильная дата");
                return;
            }
            (patient.LastName, patient.MiddleName, patient.FirstName, patient.Diagnosis, patient.Cure) = (LastName.Text, MiddleName.Text, FirstName.Text, Diagnosis.Text, Cure.Text);

            if (patient.Id == 0)
                dataContainer.Attach(patient);
            else
                dataContainer.Update(patient);

            await dataContainer.SaveChangesAsync();

            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }

        private void Go_Back(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }
    }
}
