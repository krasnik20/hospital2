﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class EditDoctor : Page
    {
        private Doctor doctor = new Doctor();
        private DataContainer dataContainer;

        public EditDoctor()
        {
            InitializeComponent();
            dataContainer = new DataContainer();
        }
        public int DoctorId
        {
            get
            {
                return doctor.Id;
            }
            set
            {
                UpdateFields(value);
            }
        }

        private async Task UpdateFields(int value)
        {
            doctor = await dataContainer.Doctors.FirstOrDefaultAsync(p => p.Id == value);
            (LastName.Text, MiddleName.Text, FirstName.Text, Speciality.Text, Description.Text) = (doctor.LastName, doctor.MiddleName, doctor.FirstName, doctor.Speciality, doctor.Description);
            Footer.CanDelete = true;
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            (doctor.LastName, doctor.MiddleName, doctor.FirstName, doctor.Speciality, doctor.Description) = (LastName.Text, MiddleName.Text, FirstName.Text, Speciality.Text, Description.Text);

            if (doctor.Id == 0)
                dataContainer.Attach(doctor);
            else
                dataContainer.Update(doctor);

            await dataContainer.SaveChangesAsync();

            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }

        private void Go_Back(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }
    }
}
