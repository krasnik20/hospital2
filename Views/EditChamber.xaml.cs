﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class EditChamber : Page
    {
        public EditChamber()
        {
            InitializeComponent();
            dataContainer = new DataContainer();
        }

        private Chamber chamber = new Chamber();
        private DataContainer dataContainer;

        public int ChamberId
        {
            get
            {
                return chamber.Id;
            }
            set
            {
                UpdateFields(value);
            }
        }

        private async Task UpdateFields(int value)
        {
            chamber = await dataContainer.Chambers.FirstOrDefaultAsync(c => c.Id == value);
            await dataContainer.Entry(chamber).Reference(p => p.Doctor).LoadAsync();
            Number.Text = chamber.Number;
            DoctorSelection.Text = chamber.Doctor?.GetName() ?? "Не выбран";
            Footer.CanDelete = true;
        }

        private async void Doctor_EditPressed(object sender, EventArgs e)
        {
            var page = new DoctorsPage() { SelectionMode = true };
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(page);
            var doctor = await page.Result.Task;
            if (doctor == null) return;
            chamber.Doctor = doctor;
            DoctorSelection.Text = doctor.GetName();
        }

        private void Doctor_RemovePressed(object sender, EventArgs e)
        {
            chamber.Doctor = null;
            DoctorSelection.Text = "Не выбран";
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            chamber.Number = Number.Text;

            if (chamber.Id == 0)
                dataContainer.Attach(chamber);
            else
                dataContainer.Update(chamber);

            await dataContainer.SaveChangesAsync();

            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }

        private void Go_Back(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }
    }
}
