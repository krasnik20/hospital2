﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class ChambersPage : Page
    {
        private DataContainer dataContainer;
        private bool selectionMode;

        public bool SelectionMode 
        {
            get
            {
                return selectionMode;
            }
            set
            {
                selectionMode = value;
                BackButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                Header.Text = value ? "Выберите палату" : "Список палат";
                CreateButton.Visibility = !value ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public TaskCompletionSource<Chamber> Result { get; set; }

        public ChambersPage()
        {
            InitializeComponent();

            dataContainer = new DataContainer();

            Loaded += async (s, e) =>
            {
                await GetItems();
            };

            Result = new TaskCompletionSource<Chamber>();

            Unloaded += (s, e) =>
            {
                if (SelectionMode) Result.TrySetResult(null);
            };
        }

        private async void Chamber_Click(object sender, RoutedEventArgs e)
        {
            var navigation = (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService;
            var chamberId = (int)(sender as Button).Tag;
            if (SelectionMode)
            {
                Result.TrySetResult(await dataContainer.Chambers.FirstOrDefaultAsync(d => d.Id == chamberId));
                navigation.GoBack();
            }
            else
            {
                navigation.Navigate(new EditChamber() { ChamberId = chamberId });
            }
        }

        private async Task GetItems()
        {
            List.ItemsSource = await dataContainer.Chambers.AsNoTracking().OrderBy(c => c.Number).Skip((Paginator.PageNumber - 1) * 10).Take(Paginator.PageNumber * 10).Select(c => new { Id = c.Id, Number = c.GetNumber(), Doctor = c.Doctor.GetName() }).ToListAsync();
        }

        private async void Paginator_PageChanged(object sender, EventArgs e)
        {
            await GetItems();
        }

        private async void Refresh_Click(object sender, RoutedEventArgs e)
        {
            await GetItems();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(new EditChamber());
        }

        private void Go_Back(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }
    }
}
