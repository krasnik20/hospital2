﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class PatientsPage : Page
    {
        DataContainer dataContainer;

        public PatientsPage()
        {
            InitializeComponent();

            dataContainer = new DataContainer();

            Loaded += async (s, e) =>
            {
                await GetItems();
            };
        }

        private void Patient_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(new EditPatient(){ PatientId = (int)(sender as Button).Tag });
        }

        private async void Paginator_PageChanged(object sender, System.EventArgs e)
        {
            await GetItems();
        }

        private async void Refresh_Click(object sender, RoutedEventArgs e)
        {
            await GetItems();
        }

        private async Task GetItems()
        {
            List.ItemsSource = await dataContainer.Patients.AsNoTracking().OrderByDescending(p => p.Arrival).Skip((Paginator.PageNumber - 1) * 10).Take(Paginator.PageNumber * 10).Select(p => new { Id = p.Id, Name = p.GetName(), Chamber = p.Chamber.GetNumber(), Doctor = p.Doctor.GetName() }).ToListAsync();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(new EditPatient());
        }
    }
}
