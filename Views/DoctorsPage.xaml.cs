﻿using Microsoft.EntityFrameworkCore;
using Practice.Data;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Practice.Views
{
    public partial class DoctorsPage : Page
    {
        private DataContainer dataContainer;

        private bool selectionMode;

        public bool SelectionMode
        {
            get
            {
                return selectionMode;
            }
            set
            {
                selectionMode = value;
                BackButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                Header.Text = value ? "Выберите врача" : "Врачи";
                CreateButton.Visibility = !value ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public TaskCompletionSource<Doctor> Result { get; set; }

        public DoctorsPage()
        {
            InitializeComponent();

            dataContainer = new DataContainer();

            Loaded += async (s, e) =>
            {
                await GetItems();
            };

            Result = new TaskCompletionSource<Doctor>();

            Unloaded += (s, e) =>
            {
                if (SelectionMode) Result.TrySetResult(null);
            };
        }

        private async void Doctor_Click(object sender, RoutedEventArgs e)
        {
            var navigation = (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService;
            var doctorId = (int)(sender as Button).Tag;
            if (SelectionMode)
            {
                Result.TrySetResult(await dataContainer.Doctors.FirstOrDefaultAsync(d => d.Id == doctorId));
                navigation.GoBack();
            }
            else
            {
                navigation.Navigate(new EditDoctor() { DoctorId = doctorId });
            }
        }

        private async Task GetItems()
        {
            List.ItemsSource = await dataContainer.Doctors.AsNoTracking().OrderBy(c => c.LastName).ThenBy(c => c.FirstName).Skip((Paginator.PageNumber - 1) * 10).Take(Paginator.PageNumber * 10).Select(d => new { Id = d.Id, Name = d.GetName(), Speciality = d.Speciality }).ToListAsync();
        }

        private async void Paginator_PageChanged(object sender, EventArgs e)
        {
            await GetItems();
        }

        private async void Refresh_Click(object sender, RoutedEventArgs e)
        {
            await GetItems();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.Navigate(new EditDoctor());
        }

        private void Go_Back(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow as MainWindow).MainFrame.NavigationService.GoBack();
        }
    }
}
